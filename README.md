# Symfony Blog With Unit and Functional testing

A blog that was created in symfony 5.0.8 that has a lot of cool features to it.

### Features

* multilevel nested categories tree
* user can sort posts 'ASC' 'DESC' 'by post rating(by Likes)'
* user can't comment unless he is logged in 
* user can register
* user can login
* user can like posts
* user can unlike posts
* user can dislike posts
* user can undislike posts
* user can comment and read other users comments
* user can delete his comment
* user has his own dashboard
* user can update his profile
* user can delete his account
* user can keep track on posts his liked

* App has two roles admin and super admin
* su admin can do all the above as a normal user
* su admin can create posts
* su admin can delete posts
* su admin can edit posts
* su admin add categories
* can edit categories
* su admin can delete categories
* su admin can rename and change parrent id for categories
* su admin can see users 
* su admin can remove users

* posts have images
* posts images are saved in the public/uploads folder
* after remove posts images will be deleted from folder accordingly

* all functions are tested with PHPUnit Bridge
* project deployed to heroku

### Installing

https://github.com/letowebdev/Symfony_Blog.git


## Website

* https://www.youtube.com/watch?v=eKe_8WAfZX0
* https://zacheleto.me/Projects/Symfony_Blog/public/

## Built With

* [symfony 5.0.8]

## Author

* **Zache Abdelatif (Leto)** 

## License

This project is licensed under the MIT License
